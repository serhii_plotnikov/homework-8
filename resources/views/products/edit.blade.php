@extends('layouts.app')

@section('title',$title)

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-5">
            <form role="form" method="post" action="{{route('products.update',['product'=>$product])}}">
                @method('put')
                @csrf
                <div class="form-group">
                    <label for="nameProduct">Name product</label>
                    <input type="text" class="form-control" name="name" id="nameProduct"
                           value="{{$product->name}}" placeholder="Enter name">
                </div>
                <div class="form-group">
                    <label for="short_name">Price</label>
                    <input type="text" class="form-control" name="price" id="price"
                           value="{{$product->price}}" placeholder="Enter price">
                </div>
                <button type="submit" class="btn btn-primary">Save</button>
            </form>
        </div>
    </div>
@endsection