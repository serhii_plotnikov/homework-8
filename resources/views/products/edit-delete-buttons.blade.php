<td class="text-center">
    <a href="{{route('products.edit',['id'=>$productId])}}" class="btn btn-primary btn-sm"
       role="button">Edit</a>
</td>
<td class="text-center">
    <form method="post" action="{{route('products.destroy',['id'=>$productId])}}">
        @method('delete')
        @csrf
        <input type="submit" class="btn-sm btn btn-primary" value="Delete">
    </form>
</td>