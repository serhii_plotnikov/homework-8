@extends('layouts.app')

@section('title',$title)

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">Price</th>
                        @can('show-edit-delete-buttons',$product)
                            <th scope="col">Edit</th>
                            <th scope="col">Delete</th>
                        @endcan
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th scope="row">{{$product->id}}</th>
                        <td>{{$product->name}}</td>
                        <td>{{$product->price}}</td>
                        @can('show-edit-delete-buttons',$product)
                            @component('products.edit-delete-buttons',['productId' => $product->id])
                            @endcomponent
                        @endcan
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection