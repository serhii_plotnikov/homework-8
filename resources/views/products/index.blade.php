@extends('layouts.app')

@section('title',$title)

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">Price</th>
                        <th scope="col">Edit</th>
                        <th scope="col">Delete</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($products as $product)
                        <tr>
                            <th scope="row">{{$product->id}}</th>
                            <td><a href="{{route('products.show',['product'=>$product->id])}}">
                                    {{$product->name}}</a>
                            </td>
                            <td>{{$product->price}}</td>
                            @can('show-edit-delete-buttons',$product)
                                @component('products.edit-delete-buttons',['productId' => $product->id])
                                @endcomponent
                            @endcan
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
