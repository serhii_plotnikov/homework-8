<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'ProductController@main')->name('home');
Auth::routes();

Route::resource('products', 'ProductController')->names([
    'create' => 'products.add'
])->middleware('auth');

Route::get('auth/facebook','Auth\FacebookController@redirectToFacebook')->name('auth-facebook');
Route::get('auth/facebook/callback', 'Auth\FacebookController@handleFacebookCallback');
