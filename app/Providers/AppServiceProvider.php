<?php

namespace App\Providers;

use App\Services\FacebookService;
use App\Services\FacebookServiceInterface;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Route::resourceVerbs([
            'create' => 'add'
        ]);
        $this->app->singleton(FacebookServiceInterface::class,FacebookService::class);
    }
}
