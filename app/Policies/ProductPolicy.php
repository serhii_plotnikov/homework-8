<?php

namespace App\Policies;

use App\Entity\User;
use App\Entity\Product;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProductPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can update the product.
     *
     * @param \App\Entity\User $user
     * @param \App\Product $product
     * @return mixed
     */
    public function update(User $user, Product $product)
    {
        return $user->is_admin || $user->id === $product->user_id;
    }

    /**
     * Determine whether the user can delete the product.
     *
     * @param \App\Entity\User $user
     * @param \App\Entity\Product $product
     * @return mixed
     */
    public function delete(User $user, Product $product)
    {
        return $user->is_admin || $user->id === $product->user_id;
    }


    public function showEditDeleteButtons(User $user, Product $product)
    {
        return $user->is_admin || $user->id === $product->user_id;
    }

}
