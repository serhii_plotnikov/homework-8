<?php

namespace App\Http\Controllers\Auth;

use App\Services\FacebookServiceInterface;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class FacebookController extends Controller
{
    public function redirectToFacebook()
    {
        return Socialite::driver('facebook')->redirect();
    }

    public function handleFacebookCallback(FacebookServiceInterface $facebookService)
    {
        try {
            $facebookUser = Socialite::driver('facebook')->user();
            $user = $facebookService->findOrCreateUser($facebookUser);
            Auth::loginUsingId($user->id);
            return redirect()->route('home');
        } catch (\Exception $exception) {
            return redirect()->route('auth-facebook');
        }
    }
}
