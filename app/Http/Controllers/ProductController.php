<?php

namespace App\Http\Controllers;

use App\Entity\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class ProductController extends Controller
{
    public function main()
    {
        return view('main', ['title' => 'Marketplace']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        return view('products.index', ['products' => $products, 'title' => 'List of products']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('products.add', ['title' => 'Create product']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Product::create(['name' => $request->name, 'price' => $request->price, 'user_id' => $request->user()->id]);
        return redirect()->route('products.index');
    }

    /**
     * Display the specified resource.
     *
     * @param Product $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return view('products.show', ['product' => $product, 'title' => 'Information about product']);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Product $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        if (Gate::allows('update', $product)) {
            return view('products.edit', ['product' => $product, 'title' => 'Edit product']);
        }
        return redirect()->route('home');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param Product $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        if (Gate::denies('update', $product)) {
            return redirect()->route('home');
        }
        $product->name = $request->name;
        $product->price = $request->price;
        $product->save();
        return redirect()->route('products.show', ['product' => $product->id]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Product $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        if (Gate::denies('delete', $product)) {
            return redirect()->route('home');
        }
        $product->delete();
        return redirect()->route('products.index');
    }
}
