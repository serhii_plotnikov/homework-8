<?php


namespace App\Services;


use App\Entity\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Laravel\Socialite\Two\User as FacebookUser;

class FacebookService implements FacebookServiceInterface
{
    public function findOrCreateUser(FacebookUser $facebookUser): User
    {
        $user = User::where('facebook_id', $facebookUser->getId())->first();
        if (is_null($user)) {
            $user = User::create(['name' => $facebookUser->getName(), 'email' => $facebookUser->getEmail(),
                'facebook_id' => $facebookUser->getId(), 'password' => Hash::make(Str::random(60))]);
        }
        return $user;
    }

}