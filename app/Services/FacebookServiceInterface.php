<?php


namespace App\Services;



use App\Entity\User;
use Laravel\Socialite\Two\User as FacebookUser;


interface FacebookServiceInterface
{
    public function findOrCreateUser(FacebookUser $facebookUser): User;
}